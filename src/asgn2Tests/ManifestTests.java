package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 * QUTU7200318
 * IBMU4882351
 */

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;
import static org.junit.Assert.*;

/**
 * @author Chun Wai Chow
 */

public class ManifestTests {
	/*
	 * ----------------------------------------------------- Basic tests
	 * -----------------------------------------------------
	 */

	/*
	 * declare Cargo Manifest Objects All basic Container code will be using
	 * MSCU6639871
	 */
	CargoManifest cargoManifest;

	/**
	 * Test that a CargoManifest can be created
	 * 
	 * @throws ManifestException
	 */
	@Test
	public void newCargoManifest() throws ManifestException {
		cargoManifest = new CargoManifest(5, 3, 30);
	}

	/**
	 * Test that loadContainer loads a container
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void loadContainer() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		cargoManifest.loadContainer(container);
		assertEquals(0, cargoManifest.whichStack(code).intValue());
		assertEquals(0, cargoManifest.howHigh(code).intValue());
	}

	/**
	 * Test that unloadContainer unloads a container
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void unloadContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		cargoManifest.loadContainer(container);
		assertEquals(0, cargoManifest.whichStack(code).intValue());
		assertEquals(0, cargoManifest.howHigh(code).intValue());
		cargoManifest.unloadContainer(code);
		assertNull(cargoManifest.whichStack(code));
	}

	/**
	 * Test that whichStack returns the stack containing a particular container.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */

	@Test
	public void whichStack() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		ContainerCode code2 = new ContainerCode("QUTU7200318");
		RefrigeratedContainer container2 = new RefrigeratedContainer(code2, 5,
				4);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		assertEquals(0, cargoManifest.whichStack(code).intValue());
		assertEquals(1, cargoManifest.whichStack(code2).intValue());
	}

	/**
	 * Test that howHigh returns the height of a container in a stack.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void howHigh() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		ContainerCode code2 = new ContainerCode("QUTU7200318");
		GeneralGoodsContainer container2 = new GeneralGoodsContainer(code2, 5);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		assertEquals(0, cargoManifest.howHigh(code).intValue());
		assertEquals(1, cargoManifest.howHigh(code2).intValue());
	}

	/**
	 * Test that toArray returns and array of the specified stack.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void toArray() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		ContainerCode code2 = new ContainerCode("QUTU7200318");
		GeneralGoodsContainer container2 = new GeneralGoodsContainer(code2, 5);

		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);

		FreightContainer[] containers = cargoManifest.toArray(0);
		assertEquals(containers[0], container);
		assertEquals(containers[1], container2);
	}

	/*
	 * ----------------------------------------------------- Exception testing
	 * -----------------------------------------------------
	 */

	/**
	 * Test that a container loaded with loadContainer does not exceed the ships
	 * weight limit throwing a ManifestException.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void exceedLoadingOfShip() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 20);
		ContainerCode code2 = new ContainerCode("QUTU7200318");
		GeneralGoodsContainer container2 = new GeneralGoodsContainer(code2, 20);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
	}

	/**
	 * Check that the new container is loaded as close to the bridge as
	 * possible.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void closeToTheBridge() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		ContainerCode code2 = new ContainerCode("QUTU7200318");
		RefrigeratedContainer container2 = new RefrigeratedContainer(code2, 5,
				4);
		ContainerCode code3 = new ContainerCode("IBMU4882351");
		GeneralGoodsContainer container3 = new GeneralGoodsContainer(code3, 5);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.loadContainer(container3);
		assertEquals(0, cargoManifest.whichStack(code).intValue());
		assertEquals(0, cargoManifest.whichStack(code3).intValue());
		assertEquals(1, cargoManifest.whichStack(code2).intValue());
	}

	/**
	 * Test that a container being loaded with loadContainer does not exceed the
	 * stack height limit and throws a ManifestException i.e. there is a place
	 * to load the container. Container Code: QUTU7200318, MSCU6639871,
	 * IBMU4882351 Same type in a same stack but over the height
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void exceedStackHeight() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(2, 2, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		ContainerCode code2 = new ContainerCode("QUTU7200318");
		RefrigeratedContainer container2 = new RefrigeratedContainer(code2, 5,
				4);
		ContainerCode code3 = new ContainerCode("IBMU4882351");
		GeneralGoodsContainer container3 = new GeneralGoodsContainer(code3, 5);
		ContainerCode code4 = new ContainerCode("CSQU3054389");
		GeneralGoodsContainer container4 = new GeneralGoodsContainer(code4, 5);

		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.loadContainer(container3);
		cargoManifest.loadContainer(container4);
	}

	/**
	 * Check that a container with the same code as one already loaded cannot be
	 * loaded onto the ship with loadContainer and throws a ManifestException.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void duplicateContainerCode() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		GeneralGoodsContainer container2 = new GeneralGoodsContainer(code, 5);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
	}

	/**
	 * Check that only containers of the same type are placed in the same stack.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void sameTypeInSameStack() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		ContainerCode code3 = new ContainerCode("IBMU4882351");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		RefrigeratedContainer container2 = new RefrigeratedContainer(code2, 5,
				4);
		GeneralGoodsContainer container3 = new GeneralGoodsContainer(code3, 5);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.loadContainer(container3);
		assertEquals(0, cargoManifest.whichStack(code).intValue());
		assertEquals(1, cargoManifest.whichStack(code2).intValue());
		assertEquals(0, cargoManifest.whichStack(code3).intValue());
	}

	/**
	 * Check that when unloadContainer is used ManifestException is thrown if
	 * the container is not accessible i.e. not on the top of the stack.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void containerOnTheBottom() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		GeneralGoodsContainer container2 = new GeneralGoodsContainer(code2, 5);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.unloadContainer(code);
	}

	/**
	 * Check that when unloadContainer is used ManifestException is thrown if
	 * the container does not exist.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test(expected = ManifestException.class)
	public void unloadANonExistContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 5);
		cargoManifest.loadContainer(container);
		cargoManifest.unloadContainer(code2);
	}
	
	/**
	 * Check that ManifestException is thrown if a negative number is given for
	 * numStacks in the constructor.
	 * 
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void constructorWithNegativeNumStacks() throws ManifestException {
		cargoManifest = new CargoManifest(-5, 3, 30);
	}

	/**
	 * Check that ManifestException is thrown if a negative number is given for
	 * maxHeight in the constructor.
	 * 
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void constructorWithNegativeMaxHeight() throws ManifestException {
		cargoManifest = new CargoManifest(5, -3, 30);
	}

	/**
	 * Check that ManifestException is thrown if a negative number is given for
	 * maxWeight in the constructor.
	 * 
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void constructorWithNegativeMaxWeight() throws ManifestException {
		cargoManifest = new CargoManifest(5, 3, -30);
	}

	/**
	 * Check that whichStack returns null if the container is not on board.
	 */
	@Test
	public void checkStackWithNoContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		assertNull(cargoManifest.whichStack(code));
	}

	/**
	 * Check that howHigh returns null if the container is not on board.
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void checkHighWithNoContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(5, 3, 30);
		ContainerCode code = new ContainerCode("MSCU6639871");
		assertNull(cargoManifest.howHigh(code));
	}

	/**
	 * Check that toArray throws a ManifestException if there is no such stack
	 * on the ship.
	 * 
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void checkNoContainer() throws ManifestException {
		cargoManifest = new CargoManifest(5, 3, 30);
		cargoManifest.toArray(7);
	}

	/*
	 * -------------------------------Other testing or Extra Exception testing
	 * -----------------------------------------------------
	 */

	/**
	 * create a manifest with weight which less than a standard weight of
	 * container this cargo cannot carry anything
	 * 
	 * @throws ManifestException
	 */

	@Test(expected = ManifestException.class)
	public void manifestSmallThanMinimunWeightOfContainer()
			throws ManifestException, InvalidContainerException,
			InvalidCodeException {
		cargoManifest = new CargoManifest(5, 3, 3);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);
		cargoManifest.loadContainer(container);
	}

	/**
	 * Create a manifest with 0 stacks, height and weight but no cargo manifest
	 * like this
	 * 
	 * @throws ManifestException
	 */
	@Test
	public void manifestCreateWithAllZero() throws ManifestException {
		cargoManifest = new CargoManifest(0, 0, 0);
	}
	
	/**
	 * Create a manifest with negative stacks, height and weight but no cargo manifest
	 * like this
	 * 
	 * @throws ManifestException
	 */
	@Test(expected = ManifestException.class)
	public void manifestCreateWithAllNegative() throws ManifestException {
		cargoManifest = new CargoManifest(-1,-1,-1);
	}

	/**
	 * Add each type of container
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void addOneOfEach() throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		cargoManifest = new CargoManifest(3, 3, 110);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container2 = new DangerousGoodsContainer(code2,
				5, 1);
		ContainerCode code3 = new ContainerCode("IBMU4882351");
		RefrigeratedContainer container3 = new RefrigeratedContainer(code3, 4,
				1);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.loadContainer(container3);
		assertEquals(0, cargoManifest.whichStack(code).intValue());
		assertEquals(1, cargoManifest.whichStack(code2).intValue());
		assertEquals(2, cargoManifest.whichStack(code3).intValue());
	}

	/**
	 * Add each type of containers then Unload the middle container
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void addOneOfEachThenUnloadMiddleContainer()
			throws ManifestException, InvalidCodeException,
			InvalidContainerException {
		cargoManifest = new CargoManifest(3, 3, 110);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container2 = new DangerousGoodsContainer(code2,
				5, 1);
		ContainerCode code3 = new ContainerCode("IBMU4882351");
		RefrigeratedContainer container3 = new RefrigeratedContainer(code3, 4,
				1);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.loadContainer(container3);
		cargoManifest.unloadContainer(code2);
		assertEquals(0, cargoManifest.whichStack(code).intValue());
		assertNull(cargoManifest.whichStack(code2));
		assertEquals(2, cargoManifest.whichStack(code3).intValue());
	}

	/**
	 * Unload a container, then add another one which will replace for the first
	 * stack
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void loadContainerAfterUnloadAContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(3, 3, 110);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container2 = new DangerousGoodsContainer(code2,
				5, 1);
		ContainerCode code3 = new ContainerCode("IBMU4882351");
		GeneralGoodsContainer container3 = new GeneralGoodsContainer(code3, 4);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.unloadContainer(code);
		cargoManifest.loadContainer(container3);
		assertEquals(1, cargoManifest.whichStack(code2).intValue());
		assertEquals(0, cargoManifest.whichStack(code3).intValue());
	}

	/**
	 * load different types of container general and dangerous then unload
	 * general container and add one more dangerous container
	 * 
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void sameTypeButNotSameStack() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(3, 3, 110);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container2 = new DangerousGoodsContainer(code2,
				5, 1);
		ContainerCode code3 = new ContainerCode("IBMU4882351");
		DangerousGoodsContainer container3 = new DangerousGoodsContainer(code3,
				4, 2);
		cargoManifest.loadContainer(container);
		cargoManifest.loadContainer(container2);
		cargoManifest.unloadContainer(code);
		cargoManifest.loadContainer(container3);
		assertEquals(1, cargoManifest.whichStack(code2).intValue());
		assertEquals(0, cargoManifest.whichStack(code3).intValue());
	}
	
	/**
	 * find high of non-exist container
	 * return null
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void findHowHighOfNonExistContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(3, 3, 110);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);
		cargoManifest.loadContainer(container);
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		assertNull(cargoManifest.howHigh(code2));
	}
	
	/**
	 * find stack of non-exist container
	 * return null
	 * @throws ManifestException
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	@Test
	public void findWhichStackOfNonExistContainer() throws ManifestException,
			InvalidCodeException, InvalidContainerException {
		cargoManifest = new CargoManifest(3, 3, 110);
		ContainerCode code = new ContainerCode("MSCU6639871");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);
		cargoManifest.loadContainer(container);
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		assertNull(cargoManifest.whichStack(code2));
	}

}
