package asgn2Tests;

/* Some valid container codes used in the tests below:
 * INKU2633836
 * KOCU8090115
 * MSCU6639871
 * CSQU3054389
 */

import static org.junit.Assert.*;

import org.junit.Test;

import asgn2Codes.ContainerCode;
import asgn2Containers.DangerousGoodsContainer;
import asgn2Containers.FreightContainer;
import asgn2Containers.GeneralGoodsContainer;
import asgn2Containers.RefrigeratedContainer;
import asgn2Exceptions.InvalidCodeException;
import asgn2Exceptions.InvalidContainerException;

/**
 * Tests for the container classes.
 * @author Kyle Stevens
 *
 */

public class ContainerTests {
	//Includes tests for ContainerCode and for the actual container classes. 
	/*
	 * -----------------------------------------------------
	 * Basic tests
	 * -----------------------------------------------------
	 */
	
	/*
	 * ------------- ContainerCode -------------
	 */

	/**
	 * Check that a valid ContainerCode can be constructed.
	 * test with 
	 * INKU2633836
	 * KOCU8090115
	 * MSCU6639871
	 * CSQU3054389
	 * 
	 * @throws InvalidCodeException
	 */
	@Test
	public void createCodeTest() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("INKU2633836");
		ContainerCode code2 = new ContainerCode("KOCU8090115");
		ContainerCode code3 = new ContainerCode("MSCU6639871");
		ContainerCode code4 = new ContainerCode("CSQU3054389");
		
		
		assertEquals("INKU2633836", code.toString());
		assertEquals("KOCU8090115", code2.toString());
		assertEquals("MSCU6639871", code3.toString());
		assertEquals("CSQU3054389", code4.toString());
	}
	
	/**
	 * Check that equals returns true if the two objects(Strings)
	 * are the same and false if they are different.
	 * test with
	 * INKU2633836 and INKU2633836
	 * KOCU8090115 and INKU2633836
	 * 
	 * @throws InvalidCodeException
	 */
	
	@Test
	public void testEqualsTrue() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("INKU2633836");
		ContainerCode code2 = new ContainerCode("INKU2633836");
		
		
		assertTrue(code.equals(code2));
	}
	
	@Test
	public void testEqualsFalse() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		ContainerCode code2 = new ContainerCode("INKU2633836");
		
		
		assertFalse(code.equals(code2));
	}
	
	/*
	 * ------------- FreightContainer -------------
	 * Abstract class so no need to test (can't instantiate), testing implementations
	 */
	
	/*
	 * ------------- DangerousGoodsContainer -------------
	 */
	
	/**
	 * Check that a valid DangerousGoodsContainer can be created.
	 * test with
	 * DangerousGoodsContainer("INKU2633836", 8, 3)
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void createDangerousContainer() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 10, 8);

		
		
		assertEquals("KOCU8090115", container.getCode().toString());
		assertEquals(10, container.getGrossWeight().intValue());
		assertEquals(8, container.getCategory().intValue());
	}
	
	/**
	 * Check that a grossWeight of 30 can be entered.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void DangerousContainerWeightHigh() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 30, 8);

		assertEquals(30, container.getGrossWeight().intValue());
		
	}
	
	/**
	 * Check that a grossWeight of 4 can be entered.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void DangerousContainerWeightLow() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 4, 8);

		assertEquals(4, container.getGrossWeight().intValue());
		
	}
	
	/**
	 * Check that getCategory returns the category of the dangerous goods when high (9).
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void getDangerousContainerCategoryHigh() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 10, 9);

		assertEquals(9, container.getCategory().intValue());
	}
	
	/**
	 * Check that getCategory returns the category of the dangerous goods when low (1).
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void getDangerousContainerCategoryLow() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 10, 1);

		assertEquals(1, container.getCategory().intValue());
	}
	
	/*
	 * ------------- GeneralGoodsContainer -------------
	 */
	
	/**
	 * Check that a valid GeneralGoodsContainer can be created.
	 * test with
	 * GeneralGoodsContainer("CSQU3054389", 7)
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void createGeneralContainer() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 10);

		
		
		assertEquals("KOCU8090115", container.getCode().toString());
		assertEquals(10, container.getGrossWeight().intValue());
	}
	
	/**
	 * Check that a grossWeight of 30 can be entered.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void GeneralContainerWeightHigh() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 30);

		assertEquals(30, container.getGrossWeight().intValue());
		
	}
	
	/**
	 * Check that a grossWeight of 4 can be entered.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void GeneralContainerWeightLow() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 4);

		assertEquals(4, container.getGrossWeight().intValue());
		
	}
	
	/*
	 * ------------- RefrigeratedContainer -------------
	 */
	
	/**
	 * Check that a valid RefrigeratedContainer can be created.
	 * test with
	 * RefrigeratedContainer("MSCU6639871", 10, -5)
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void createRefrigeratedContainer() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		RefrigeratedContainer container = new RefrigeratedContainer(code, 10, -5);

		
		
		assertEquals("KOCU8090115", container.getCode().toString());
		assertEquals(10, container.getGrossWeight().intValue());
		assertEquals(-5, container.getTemperature().intValue());
	}
	
	
	/**
	 * Check that a grossWeight of 30 can be entered.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void RefrigeratedContainerWeightHigh() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		RefrigeratedContainer container = new RefrigeratedContainer(code, 30, -5);

		assertEquals(30, container.getGrossWeight().intValue());
		
	}
	
	/**
	 * Check that a grossWeight of 4 can be entered.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void RefrigeratedContainerWeightLow() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		RefrigeratedContainer container = new RefrigeratedContainer(code, 4, -5);

		assertEquals(4, container.getGrossWeight().intValue());
		
	}
	
	/**
	 * Check that getTemperature returns the current temperature
	 * of the container.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void RefrigeratedGetTemperature() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		RefrigeratedContainer container = new RefrigeratedContainer(code, 10, -5);
		container.setTemperature(7);

		assertEquals(7, container.getTemperature().intValue());
	}
	
	/**
	 * Check that setTemperature sets the temperature of the
	 * container zero.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void RefrigeratedSetTempZero() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		RefrigeratedContainer container = new RefrigeratedContainer(code, 10, -5);
		container.setTemperature(0);

		assertEquals(0, container.getTemperature().intValue());
	}
	
	/**
	 * Check that setTemperature sets the temperature of the
	 * container positive.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void RefrigeratedSetTempPositive() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		RefrigeratedContainer container = new RefrigeratedContainer(code, 10, -5);
		container.setTemperature(15);

		assertEquals(15, container.getTemperature().intValue());
	}
	
	/**
	 * Check that setTemperature sets the temperature of the
	 * container negative.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test
	public void RefrigeratedSetTempNegative() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		RefrigeratedContainer container = new RefrigeratedContainer(code, 10, 5);
		container.setTemperature(-15);

		assertEquals(-15, container.getTemperature().intValue());
	}
	
	/*
	 * -----------------------------------------------------
	 * Exception testing
	 * -----------------------------------------------------
	 */
	
	/*
	 * ------------- ContainerCode -------------
	 */
	
	/**
	 * Check that when ContainerCode is given an invalid Owner Code
	 * it throws an InvalidCodeException. test with inkU2633836
	 * 
	 * @throws InvalidCodeException
	 * 
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidCodeException.class)
	public void InvalidCodeOwner() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("inkU2633836");
	}
	
	/**
	 * Check that when ContainerCode is given a Category Identifier
	 * that is not 'U' it throws an InvalidCodeException.
	 * test with KOCu8090115 and KOCA8090115
	 * 
	 * @throws InvalidCodeException
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidCodeException.class)
	public void InvalidCodeCategoryIdentifierOne() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCu8090115");
	}
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidCodeException.class)
	public void InvalidCodeCategoryIdentifierTwo() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCA8090115");
	}
	
	/**
	 * Check that when ContainerCode is given a code that is
	 * not eleven characters long it throws an InvalidCodeException.
	 * test with MSCU66398711
	 * 
	 * @throws InvalidCodeException
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidCodeException.class)
	public void InvalidCodeLength() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("MSCU66398711");
	}
	
	/**
	 * Check that when ContainerCode is given a serial number that is
	 * not six digits it throws an InvalidCodeException.
	 * test with CSQU305a389
	 * 
	 * @throws InvalidCodeException
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidCodeException.class)
	public void InvalidCodeDigits() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("CSQU305a389");
	}
	
	/**
	 * Check that when ContainerCode is given "" (empty) it throws
	 * an InvalidCodeException.
	 * test with ""
	 * 
	 * @throws InvalidCodeException
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidCodeException.class)
	public void InvalidCodeEmpty() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("");
	}
	
	/**
	 * Check that when ContainerCode is given a code with an invalid
	 * check digit it throws an InvalidCodeException.
	 * 
	 * @throws InvalidCodeException
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidCodeException.class)
	public void InvalidCodeDigit() throws InvalidCodeException {
		ContainerCode code = new ContainerCode("CSQU3054380");
	}
	
	
	/*
	 * ------------- DangerousGoodsContainer -------------
	 * Two tests below same as general not implemented
	 */
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * DangerousGoodsContainer if the grossWeight is < 4 tonnes.
	 * 
	 * 
	 */
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * DangerousGoodsContainer if the grossWeight is > 30 tonnes.
	 */
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * DangerousGoodsContainer if the category is > 9.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidContainerException.class)
	public void DangerousContainerCategoryOver() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 10, 10);
	}
	
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * DangerousGoodsContainer if the category is < 1.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidContainerException.class)
	public void DangerousContainerCategoryZero() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 10, 0);
	}
	
	@SuppressWarnings("unused")
	@Test (expected = InvalidContainerException.class)
	public void DangerousContainerCategoryNegative() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		DangerousGoodsContainer container = new DangerousGoodsContainer(code, 10, -3);
	}
	
	/*
	 * ------------- GeneralGoodsContainer -------------
	 */
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * GeneralGoodsContainer if the grossWeight is < 4 tonnes.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test (expected = InvalidContainerException.class)
	public void GeneralContainerUnderWeight() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 0);

		assertEquals(30, container.getGrossWeight().intValue());
		
	}
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * GeneralGoodsContainer if the grossWeight is > 30 tonnes.
	 * 
	 * @throws InvalidCodeException
	 * @throws InvalidContainerException
	 */
	
	@Test (expected = InvalidContainerException.class)
	public void GeneralContainerOverWeight() throws InvalidContainerException, InvalidCodeException {
		ContainerCode code = new ContainerCode("KOCU8090115");
		GeneralGoodsContainer container = new GeneralGoodsContainer(code, 100);

		assertEquals(30, container.getGrossWeight().intValue());
		
	}
	
	/*
	 * ------------- RefrigeratedContainer -------------
	 * same as general
	 */
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * RefrigeratedContainer if the grossWeight is < 4 tonnes.
	 */
	
	/**
	 * Check that an InvalidContainerException is thrown for 
	 * RefrigeratedContainer if the grossWeight is > 30 tonnes.
	 */
	
}
