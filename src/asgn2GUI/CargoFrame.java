package asgn2GUI;

import java.awt.BorderLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.HeadlessException;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import asgn2Codes.ContainerCode;
import asgn2Containers.FreightContainer;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * The main window for the Cargo Manifest graphics application.
 *
 * @author CAB302
 */
public class CargoFrame extends JFrame {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7651656039035013775L;
	private static final int WIDTH = 600;
    private static final int HEIGHT = 400;

    private JButton btnLoad;
    private JButton btnUnload;
    private JButton btnFind;
    private JButton btnNewManifest;

    private CargoCanvas canvas;

    private JPanel pnlControls;
    private JPanel pnlDisplay;

    private CargoManifest cargo;

    /**
     * Constructs the GUI.
     *
     * @param title The frame title to use.
     * @throws HeadlessException from JFrame.
     */
    public CargoFrame(String title) throws HeadlessException {
        super(title);

        constructorHelper();
        disableButtons();
        redraw();
        setVisible(true);
    }

    /**
     * Initialises the container display area.
     *
     * @param cargo The <code>CargoManifest</code> instance containing necessary state for display.
     */
    private void setCanvas(CargoManifest cargo) {
        if (canvas != null) {
            pnlDisplay.remove(canvas);
        }
        if (cargo == null) {
            disableButtons();
        } else {
            canvas = new CargoCanvas(cargo);
            pnlDisplay.add(canvas);
            enableButtons();    
        }
        redraw();
    }

    /**
     * Enables buttons for user interaction.
     */
    private void enableButtons() {
    	btnLoad.setEnabled(true);
    	btnUnload.setEnabled(true);
    	btnFind.setEnabled(true);  
    }

    /**
     * Disables buttons from user interaction.
     */
    private void disableButtons() {
    	btnLoad.setEnabled(false);
    	btnUnload.setEnabled(false);
    	btnFind.setEnabled(false);    
    }

    /**
     * Initialises and lays out GUI components.
     */
    private void constructorHelper() {
        setSize(WIDTH, HEIGHT);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setLayout( new BorderLayout() );

        btnLoad = createButton("Load", new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Runnable doRun = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.doLoad();
                    }
                };
                SwingUtilities.invokeLater(doRun);
            }
        });
        btnUnload = createButton("Unload", new ActionListener() {
        	@Override
			public void actionPerformed(ActionEvent e) {
				Runnable uLoad = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.doUnload();
                    }
                };
                SwingUtilities.invokeLater(uLoad);
				
			}    
        });
        btnFind = createButton("Find", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable find = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.doFind();
                    }
                };
                SwingUtilities.invokeLater(find);
				
			}   
        });
        btnNewManifest = createButton("New Manifest", new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				Runnable nMan = new Runnable() {
                    @Override
                    public void run() {
                        CargoFrame.this.resetCanvas();
                        CargoFrame.this.setNewManifest();
                    }
                };
                SwingUtilities.invokeLater(nMan);
			}
				
        });

        pnlControls = createControlPanel();
        
        pnlDisplay = new JPanel();
        pnlDisplay.setLayout( new BorderLayout() );
        
        CargoFrame.this.add(pnlControls, BorderLayout.SOUTH);
        
        CargoFrame.this.add(pnlDisplay, BorderLayout.CENTER);
        
        CargoFrame.this.setCanvas(cargo);        
        
        repaint();
    }

    /**
     * Creates a JPanel containing user controls (buttons).
     *
     * @return User control panel.
     */
    private JPanel createControlPanel() {
    	
    	JPanel theControl = new JPanel();
    	GridBagLayout theLayout = new GridBagLayout();
        theControl.setLayout(theLayout);
        GridBagConstraints c = new GridBagConstraints();
        
        c.insets = new Insets(5,5,5,5);
        
        c.gridx = 0;
        c.gridy = 0;
        theControl.add(btnNewManifest, c);
        
        c.gridx = 1;
        c.gridy = 0;
        theControl.add(btnLoad, c);
        
        c.gridx = 2;
        c.gridy = 0;
        theControl.add(btnUnload, c);
        
        c.gridx = 3;
        c.gridy = 0;
        theControl.add(btnFind, c);
    	
		return theControl;   
    }

    /**
     * Factory method to create a JButton and add its ActionListener.
     *
     * @param name The text to display and use as the component's name.
     * @param btnListener The ActionListener to add.
     * @return A named JButton with ActionListener added.
     */
    private JButton createButton(String name, ActionListener btnListener) {
        JButton btn = new JButton(name);
        btn.setName(name);
        btn.addActionListener(btnListener);
        return btn;
    }

    /**
     * Initiate the New Manifest dialog which sets the instance of CargoManifest to work with.
     */
    private void setNewManifest() {
    	CargoManifest man = ManifestDialog.showDialog(this);
    	
    	if(man != null) {
    		cargo = man;
    		setCanvas(cargo);
    	}   
    }

    /**
     * Turns off container highlighting when an action other than Find is initiated.
     */
    private void resetCanvas() {
    	if (canvas != null) {
    		canvas.setToFind(null);
    	}  
    }

    /**
     * Initiates the Load Container dialog.
     */
    private void doLoad() {
    	FreightContainer tempContainer = LoadContainerDialog.showDialog(this);
    	
    	// If the container wasn't found show the error message
    	if(tempContainer != null) {
    		try {
				cargo.loadContainer(tempContainer);
			} catch (ManifestException e) {
				String[] msg = e.getMessage().split(":");
				JOptionPane.showMessageDialog(getParent(), msg[1], "Invalid Container", JOptionPane.ERROR_MESSAGE);
				
			}
    	}
    	redraw();
    }

    private void doUnload() {
    	ContainerCode tempCode = ContainerCodeDialog.showDialog(this);
    	
    	// If the container wasn't found show the error message
    	if(tempCode != null) {
        try {
				cargo.unloadContainer(tempCode);
		} catch (ManifestException e) {
			String[] msg = e.getMessage().split(":");
			JOptionPane.showMessageDialog(getParent(), msg[1], "Invalid Container", JOptionPane.ERROR_MESSAGE);
			
		}
    	}
    	redraw();
    }

    private void doFind() {
    	ContainerCode tempCode = ContainerCodeDialog.showDialog(this);
    	
    	// If the container wasn't found show the error message
    	if(tempCode != null) {
    		if(cargo.howHigh(tempCode) == null){
    			JOptionPane.showMessageDialog(getParent(), "The Container Code you entered was not found", "Not Found", JOptionPane.ERROR_MESSAGE);
        	} else {
        		canvas.setToFind(tempCode);
        	}
    	}
    	redraw();
    }

    private void redraw() {
        invalidate();
        validate();
        repaint();
    }
}
