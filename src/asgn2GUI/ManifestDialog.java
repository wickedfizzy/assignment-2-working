package asgn2GUI;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import asgn2Exceptions.CargoException;
import asgn2Exceptions.ManifestException;
import asgn2Manifests.CargoManifest;

/**
 * Creates a dialog box allowing the user to enter parameters for a new
 * <code>CargoManifest</code>.
 * 
 * @author CAB302
 */
public class ManifestDialog extends AbstractDialog {

	private static final int HEIGHT = 150;
	private static final int WIDTH = 250;

	private JLabel labelNumStacks;
	private JLabel labelMaxHeight;
	private JLabel labelMaxWeight;
	private JTextField txtNumStacks;
	private JTextField txtMaxHeight;
	private JTextField txtMaxWeight;

	private CargoManifest manifest;

	/**
	 * Constructs a modal dialog box that gathers information required for
	 * creating a cargo manifest.
	 * 
	 * @param parent
	 *            the frame which created this dialog box.
	 */
	private ManifestDialog(JFrame parent) {
		super(parent, "Create Manifest", WIDTH, HEIGHT);
		setName("New Manifest");
		setResizable(false);
		manifest = null;
	}

	/**
	 * @see AbstractDialog.createContentPanel()
	 */
	@Override
	protected JPanel createContentPanel() {

		GridBagConstraints constraints = new GridBagConstraints();

		// Defaults
		constraints.fill = GridBagConstraints.NONE;
		constraints.anchor = GridBagConstraints.CENTER;
		constraints.weightx = 100;
		constraints.weighty = 100;

		labelNumStacks = new JLabel("Number of Stacks:");
		labelMaxHeight = new JLabel("Max Stack Height:");
		labelMaxWeight = new JLabel("Max Weight:");

		txtNumStacks = createTextField(8, "Number of Stacks");
		txtMaxHeight = createTextField(8, "Maximum Height");
		txtMaxWeight = createTextField(8, "Maximum Weight");

		// position setting for label
		constraints.anchor = GridBagConstraints.EAST;

		JPanel toReturn = new JPanel();
		toReturn.setLayout(new GridBagLayout());
		this.addToPanel(toReturn, labelNumStacks, constraints, 0, 0, 1, 1);
		this.addToPanel(toReturn, labelMaxHeight, constraints, 0, 1, 1, 1);
		this.addToPanel(toReturn, labelMaxWeight, constraints, 0, 2, 1, 1);

		constraints.anchor = GridBagConstraints.WEST;

		this.addToPanel(toReturn, txtNumStacks, constraints, 1, 0, 1, 1);
		this.addToPanel(toReturn, txtMaxHeight, constraints, 1, 1, 1, 1);
		this.addToPanel(toReturn, txtMaxWeight, constraints, 1, 2, 1, 1);

		return toReturn;
	}

	/*
	 * Factory method to create a named JTextField
	 */
	private JTextField createTextField(int numColumns, String name) {
		JTextField text = new JTextField();
		text.setColumns(numColumns);
		text.setName(name);
		return text;
	}

	@Override
	protected boolean dialogDone() {
		int numstacks;
		int maxHeight;
		int maxWeight; 
		try {
			if (txtNumStacks.getText().isEmpty()) {
				throw new ManifestException("Number of Stacks is empty.");
			}
			if (txtMaxHeight.getText().isEmpty()) {
				throw new ManifestException("Number of Max Height is empty.");
			}
			if (txtMaxWeight.getText().isEmpty()) {
				throw new ManifestException("Number of Max Weight is empty.");
			}
			//not sure do we need to check inputed value is int or not
			try{
			numstacks = Integer.parseInt(txtNumStacks.getText());
			maxHeight = Integer.parseInt(txtMaxHeight.getText());
			maxWeight = Integer.parseInt(txtMaxWeight.getText());
			}catch (Exception e){
				JOptionPane.showMessageDialog(this, "Please input number for all text field.",
						"Error Message", JOptionPane.ERROR_MESSAGE);
				return false;
			}
			
			manifest = new CargoManifest(numstacks, maxHeight, maxWeight);
			return true;
		} catch (ManifestException e) {
			JOptionPane.showMessageDialog(this, e.getMessage(),
					"Error Message", JOptionPane.ERROR_MESSAGE);
			return false;
		}

		// Parameters and building a new manifest, all the while handling
		// exceptions
	}

	/**
	 * Shows the <code>ManifestDialog</code> for user interaction.
	 * 
	 * @param parent
	 *            - The parent <code>JFrame</code> which created this dialog
	 *            box.
	 * @return a <code>CargoManifest</code> instance with valid values.
	 */
	public static CargoManifest showDialog(JFrame parent) {
		ManifestDialog manifestDialog = new ManifestDialog(parent);
		manifestDialog.setVisible(true);
		return manifestDialog.manifest;
	}
}
